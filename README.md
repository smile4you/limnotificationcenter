# README #

## 概要
NSNotificationCenterではNotification.userInfoの要素がAnyであるため適切にキャストする必要があります。
LimNotificationCenterではこれを改善してNotification毎のuserInfoが明確になるように設計しました。

## LICENSE
MIT License