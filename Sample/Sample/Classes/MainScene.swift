//
//  MainScene.swift
//  Sample
//
//  Created by Ysk.Manabe on 2017/12/03.
//  Copyright © 2017年 S4U. All rights reserved.
//

import UIKit
import LimNotificationCenter

class MainScene: UIViewController {
    
    private let notificationCenter = LimNotificationCenter<MainScene.Notification.Name>()
    
    private var notifications: [MainScene.Notification.Name] = []
    
    @IBOutlet var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func reload() {
        self.tableView.reloadData()
    }

    // MARK: - Insert
    @IBAction func observeToinsertButtonDidTap(_ sender: UIButton) {

        self.notificationCenter.add(observer: self) { (notification: LimNotification<MainScene.Notification.Inserted>) in
            self.notifications.insert(notification.name, at: 0)
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .fade)
        }
        
    }
    
    @IBAction func removeToInsertButtonDidTap(_ sender: UIButton) {
        
        self.notificationCenter.remove(observer: self, name: MainScene.Notification.Inserted.self)
        
    }
    
    @IBAction func notificationToInsertButtonDidTap(_ sender: UIButton) {
        
        let notification = LimNotification<MainScene.Notification.Inserted>(name: MainScene.Notification.Inserted(message: "inserted", insertedDate: Date()))
        self.notificationCenter.post(notification: notification)
    }
    
    
    // MARK: - Update
    @IBAction func observeToUpdateButtonDidTap(_ sender: UIButton) {

        self.notificationCenter.add(observer: self) { (notification: LimNotification<MainScene.Notification.Updated>) in
            self.notifications.insert(notification.name, at: 0)
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .fade)
        }

    }
    
    @IBAction func removeToUpdateButtonDidTap(_ sender: UIButton) {
        
        self.notificationCenter.remove(observer: self, name: MainScene.Notification.Updated.self)
        
    }
    
    @IBAction func notificationToUpdateButtonDidTap(_ sender: UIButton) {
        
        let numUpdated = self.notifications.numUpdated() + 1
        let notification = LimNotification<MainScene.Notification.Updated>(name: MainScene.Notification.Updated(message: "updated", updatedDate: Date(), numUpdated: numUpdated))
        self.notificationCenter.post(notification: notification)

    }
    
    
    // MARK: - All
    @IBAction func removeAllButtonDidTap(_ sender: UIButton) {
        
        self.notificationCenter.remove(observer: self)
        
    }
    
    @IBAction func removeHistoryButtonDidTap(_ sender: UIButton) {
        
        self.notifications.removeAll()
        self.reload()
        
    }
}

// MARK: - UITableViewDataSource
extension MainScene: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection sectin: Int) -> Int {
        let numberOfRows = self.notifications.count
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        guard let notificationName = self.notifications[nilable: indexPath.row] else {
            return cell
        }
        
        cell.textLabel?.text = notificationName.description.replacingOccurrences(of: ", ", with: ",\n")
        return cell
    }


}

// MARK: - UITableViewDelegate
extension MainScene: UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let notificationName = self.notifications[nilable: indexPath.row] else {
            return
        }
        
        guard let color = UIColor(named: notificationName.colorName) else {
            return
        }
        
        cell.backgroundColor = color
        
    }

}

// MARK: - Notification
extension MainScene {

    /// 通知
    public class Notification {
        
        /// 名前
        public class Name : LimNotificationNameProtocol, CustomStringConvertible {
            
            public var description: String {
                return ""
            }
            
            /// 色
            public var colorName: String {
                return "white"
            }
        }
        
        /// 追加
        public class Inserted: Name {
            
            /// メッセージ
            public let message: String
            
            /// 追加日時
            public let insertedDate: Date
            
            public override var description: String {
                let description = "{class : " + String(describing: type(of: self))
                    + ", message : " + self.message
                    + ", insertedDate : " + self.insertedDate.description
                    + "}"
                return description
            }
            
            public override var colorName: String  {
                return "header_color_insert"
            }
            
            /// 初期化
            /// - parameter message: メッセージ
            /// - parameter insertedDate: 追加日時
            public init(message: String, insertedDate: Date) {
                self.message      = message
                self.insertedDate = insertedDate
                super.init()
            }
            
            
        }
        
        /// 更新
        public class Updated: Name {
            
            /// メッセージ
            public let message: String
            
            /// 更新日時
            public let updatedDate: Date
            
            /// 更新回数
            public let numUpdated: Int
            
            public override var description: String {
                let description = "{class : " + String(describing: type(of: self))
                    + ", message : " + self.message
                    + ", updatedDate : " + self.updatedDate.description
                    + ", numUpdated : " + self.numUpdated.description
                    + "}"
                return description
            }
            
            public override var colorName: String  {
                return "header_color_update"
            }


            /// 初期化
            /// - parameter message: メッセージ
            /// - parameter updatedDate: 更新日時
            /// - parameter numUpdate: 更新回数
            public init(message: String, updatedDate: Date, numUpdated: Int) {
                self.message     = message
                self.updatedDate = updatedDate
                self.numUpdated  = numUpdated
                super.init()
            }
        }
        
    }

}
