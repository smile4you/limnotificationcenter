//
//  Array+extensions.swift
//  MilkCocoa
//
//  Created by Y.Manabe on 8/29/15.
//  Copyright (c) 2015 S4U. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    mutating func remove(by targetElement: Element) {
        for (index, element) in self.enumerated() {
            
            guard element == targetElement else {
                continue
            }
            
            self.remove(at: index)

            return
        }
    }
    
    mutating func remove<S>(contentsOf contents: S) where S: Sequence, S.Iterator.Element == Element {
        for (_, element) in contents.enumerated() {
            self.remove(by: element)
        }
    }
    
    
}

extension Array where Element: MainScene.Notification.Name {
    
    func numUpdated() -> Int {
        
        var numUpdated = 0
        
        self.forEach { (name: MainScene.Notification.Name) in
            
            if name is MainScene.Notification.Updated {
                numUpdated += 1
            }
        }
        
        return numUpdated
    }
}

extension Array {
    subscript (nilable index: Int) -> Element? {
        switch index {
        case self.indices:
            return self[index]
        default:
            return nil
        }
    }
}
