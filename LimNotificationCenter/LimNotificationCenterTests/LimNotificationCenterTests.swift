//
//  LimNotificationCenterTests.swift
//  LimNotificationCenterTests
//
//  Created by Ysk.Manabe on 2017/12/03.
//  Copyright © 2017年 S4U. All rights reserved.
//

import XCTest
@testable import LimNotificationCenter

class LimNotificationCenterTests: XCTestCase {
    
    private var notificationCenter: LimNotificationCenter<Notification.Name>!

    override func setUp() {
        super.setUp()

        self.notificationCenter = LimNotificationCenter<Notification.Name>()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // - 概要
    // 観測者登録と通知送受信と登録解除
    func test001() {
        
        // 登録
        let message = "test001"
        let insertedDate = Date()
        
        let expectation = XCTestExpectation()
        var expectation2: XCTestExpectation?
        
        self.notificationCenter.add(observer: self, notificationHandler: { (notification: LimNotification<Notification.Inserted>) in
            XCTAssertTrue(notification.name.message == message, "Message is not equals")
            XCTAssertTrue(notification.name.insertedDate == insertedDate, "Date is not equals")
            expectation.fulfill()
            if let exp2 = expectation2 {
                exp2.fulfill()
            }
        })
        
        // 通知
        let notificationName = Notification.Inserted(message: message, insertedDate: insertedDate)
        let notification = LimNotification<Notification.Inserted>(name: notificationName)
        self.notificationCenter.post(notification: notification)

        // wait
        self.wait(for: [expectation], timeout: 0.5)
        
        // 解除
        self.notificationCenter.remove(observer: self)
        
        // 通知
        self.notificationCenter.post(notification: notification)
        
        // wait
        expectation2 = XCTestExpectation()
        let result: XCTWaiter.Result = XCTWaiter().wait(for: [expectation2!], timeout: 0.5)
        switch result {
        case .completed:            // 正常
            break
        case .timedOut:             // タイムアウト
            XCTAssert(true)
            break
        case .incorrectOrder:       // 順番が違う
            break
        case .invertedFulfillment:  // 順番が逆順だった
            break
        case .interrupted:          // 中断された
            break
        }

        
    }
    
    // - 概要
    // 複数種類の通知送受信と登録解除
    func test002() {
        
        // メッセージ Insert
        let messageInserted = "insert"
        let insertedDate    = Date()
        
        let expectationInserted = XCTestExpectation(description: "Insert")
        var expectationInserted2: XCTestExpectation? = nil

        self.notificationCenter.add(observer: self, notificationHandler: { (notification: LimNotification<Notification.Inserted>) in
            XCTAssertTrue(notification.name.message == messageInserted, "Message is not equals")
            XCTAssertTrue(notification.name.insertedDate == insertedDate, "Date is not equals")
            expectationInserted.fulfill()
            if let exp2 = expectationInserted2 {
                exp2.fulfill()
            }
        })
        
        let notificationNameInserted = Notification.Inserted(message: messageInserted, insertedDate: insertedDate)
        let notificationInserted = LimNotification<Notification.Inserted>(name: notificationNameInserted)
        self.notificationCenter.post(notification: notificationInserted)
        
        // メッセージ Update
        let messageUpdated = "updated"
        let updatedDate    = Date()
        let numUpdated     = 1
        
        let expectationUpdated = XCTestExpectation(description: "Update")
        var expectationUpdated2: XCTestExpectation? = nil

        self.notificationCenter.add(observer: self) { (notification: LimNotification<Notification.Updated>) in
            XCTAssertTrue(notification.name.message == messageUpdated, "Message is not equals")
            XCTAssertTrue(notification.name.updatedDate == updatedDate, "Date is not equals")
            expectationUpdated.fulfill()
            if let exp2 = expectationUpdated2 {
                exp2.fulfill()
            }
        }
        
        let notificationNameUpdated = Notification.Updated(message: messageUpdated, updatedDate: updatedDate, numUpdated: numUpdated)
        let notificationUpdated = LimNotification<Notification.Updated>(name: notificationNameUpdated)
        self.notificationCenter.post(notification: notificationUpdated)
        
        // wait
        self.wait(for: [expectationInserted, expectationUpdated], timeout: 0.5)
        
        // 解除
        self.notificationCenter.remove(observer: self)

        // 通知
        expectationInserted2 = XCTestExpectation(description: "Inserted2")
        expectationUpdated2 = XCTestExpectation(description: "Update2")
        self.notificationCenter.post(notification: notificationInserted)
        self.notificationCenter.post(notification: notificationUpdated)
        
        // wait
        let result: XCTWaiter.Result = XCTWaiter().wait(for: [expectationInserted2!, expectationUpdated2!], timeout: 0.5)
        switch result {
        case .completed:            // 正常
            break
        case .timedOut:             // タイムアウト
            XCTAssert(true)
            break
        case .incorrectOrder:       // 順番が違う
            break
        case .invertedFulfillment:  // 順番が逆順だった
            break
        case .interrupted:          // 中断された
            break
        }

        
    }

    // - 概要
    // 複数種類の通知送受信と全解除
    func test003() {
        
        // メッセージ Insert
        let messageInserted = "insert"
        let insertedDate    = Date()
        
        let expectationInserted = XCTestExpectation(description: "Insert")
        var expectationInserted2: XCTestExpectation? = nil
        
        self.notificationCenter.add(observer: self, notificationHandler: { (notification: LimNotification<Notification.Inserted>) in
            XCTAssertTrue(notification.name.message == messageInserted, "Message is not equals")
            XCTAssertTrue(notification.name.insertedDate == insertedDate, "Date is not equals")
            expectationInserted.fulfill()
            if let exp2 = expectationInserted2 {
                exp2.fulfill()
            }
        })
        
        let notificationNameInserted = Notification.Inserted(message: messageInserted, insertedDate: insertedDate)
        let notificationInserted = LimNotification<Notification.Inserted>(name: notificationNameInserted)
        self.notificationCenter.post(notification: notificationInserted)
        
        // メッセージ Update
        let messageUpdated = "updated"
        let updatedDate    = Date()
        let numUpdated     = 1
        
        let expectationUpdated = XCTestExpectation(description: "Update")
        var expectationUpdated2: XCTestExpectation? = nil
        
        self.notificationCenter.add(observer: self) { (notification: LimNotification<Notification.Updated>) in
            XCTAssertTrue(notification.name.message == messageUpdated, "Message is not equals")
            XCTAssertTrue(notification.name.updatedDate == updatedDate, "Date is not equals")
            expectationUpdated.fulfill()
            if let exp2 = expectationUpdated2 {
                exp2.fulfill()
            }
        }
        
        let notificationNameUpdated = Notification.Updated(message: messageUpdated, updatedDate: updatedDate, numUpdated: numUpdated)
        let notificationUpdated = LimNotification<Notification.Updated>(name: notificationNameUpdated)
        self.notificationCenter.post(notification: notificationUpdated)
        
        // wait
        self.wait(for: [expectationInserted, expectationUpdated], timeout: 0.5)
        
        // Inserted解除
        self.notificationCenter.remove(observer: self, name: Notification.Inserted.self)
        
        // 通知
        expectationInserted2 = XCTestExpectation(description: "Inserted2")
        expectationUpdated2 = XCTestExpectation(description: "Update2")
        self.notificationCenter.post(notification: notificationInserted)
        self.notificationCenter.post(notification: notificationUpdated)
        
        // wait
        self.wait(for: [expectationUpdated2!], timeout: 0.5)
        let result: XCTWaiter.Result = XCTWaiter().wait(for: [expectationInserted2!], timeout: 0.5)
        switch result {
        case .completed:            // 正常
            break
        case .timedOut:             // タイムアウト
            XCTAssert(true)
            break
        case .incorrectOrder:       // 順番が違う
            break
        case .invertedFulfillment:  // 順番が逆順だった
            break
        case .interrupted:          // 中断された
            break
        }
    }
    
    
    // - 概要
    // 二重登録
    func test004() {
        
        // 登録
        let message = "test001"
        let insertedDate = Date()
        
        let expectation = XCTestExpectation()
        self.notificationCenter.add(observer: self, notificationHandler: { (notification: LimNotification<Notification.Inserted>) in
            XCTAssertTrue(notification.name.message == message, "Message is not equals")
            XCTAssertTrue(notification.name.insertedDate == insertedDate, "Date is not equals")
            expectation.fulfill()
        })

        let expectation2 = XCTestExpectation()
        self.notificationCenter.add(observer: self, notificationHandler: { (notification: LimNotification<Notification.Inserted>) in
            XCTAssertTrue(notification.name.message == message, "Message is not equals")
            XCTAssertTrue(notification.name.insertedDate == insertedDate, "Date is not equals")
            expectation2.fulfill()
        })

        // 通知
        let notificationName = Notification.Inserted(message: message, insertedDate: insertedDate)
        let notification = LimNotification<Notification.Inserted>(name: notificationName)
        self.notificationCenter.post(notification: notification)
        
        // wait
        self.wait(for: [expectation], timeout: 0.5)
        
        // 通知
        self.notificationCenter.post(notification: notification)

        // wait
        let result: XCTWaiter.Result = XCTWaiter().wait(for: [expectation2], timeout: 0.5)
        switch result {
        case .completed:            // 正常
            break
        case .timedOut:             // タイムアウト
            XCTAssert(true)
            break
        case .incorrectOrder:       // 順番が違う
            break
        case .invertedFulfillment:  // 順番が逆順だった
            break
        case .interrupted:          // 中断された
            break
        }

    }

}

/// 通知
fileprivate class Notification {
    
    /// 名前
    public class Name : LimNotificationNameProtocol, CustomStringConvertible {
        
        public var description: String {
            return ""
        }
        
        /// 色
        public var colorName: String {
            return "white"
        }
    }
    
    /// 追加
    public class Inserted: Name {
        
        /// メッセージ
        public let message: String
        
        /// 追加日時
        public let insertedDate: Date
        
        public override var description: String {
            let description = "{class : " + String(describing: type(of: self))
                + ", message : " + self.message
                + ", insertedDate : " + self.insertedDate.description
                + "}"
            return description
        }
        
        public override var colorName: String  {
            return "header_color_insert"
        }
        
        /// 初期化
        /// - parameter message: メッセージ
        /// - parameter insertedDate: 追加日時
        public init(message: String, insertedDate: Date) {
            self.message      = message
            self.insertedDate = insertedDate
            super.init()
        }
        
        
    }
    
    /// 更新
    public class Updated: Name {
        
        /// メッセージ
        public let message: String
        
        /// 更新日時
        public let updatedDate: Date
        
        /// 更新回数
        public let numUpdated: Int
        
        public override var description: String {
            let description = "{class : " + String(describing: type(of: self))
                + ", message : " + self.message
                + ", updatedDate : " + self.updatedDate.description
                + ", numUpdated : " + self.numUpdated.description
                + "}"
            return description
        }
        
        public override var colorName: String  {
            return "header_color_update"
        }
        
        
        /// 初期化
        /// - parameter message: メッセージ
        /// - parameter updatedDate: 更新日時
        /// - parameter numUpdate: 更新回数
        public init(message: String, updatedDate: Date, numUpdated: Int) {
            self.message     = message
            self.updatedDate = updatedDate
            self.numUpdated  = numUpdated
            super.init()
        }
    }
    
}
