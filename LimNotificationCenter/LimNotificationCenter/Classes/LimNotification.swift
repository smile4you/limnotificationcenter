//
//  LimNotification.swift
//  LimNotificationCenter
//
//  Created by Ysk.Manabe on 2017/12/08.
//  Copyright © 2017年 S4U. All rights reserved.
//

import Foundation

/// 通知
public class LimNotification<T: LimNotificationNameProtocol> {
    
    /// 名前
    public let name: T
    
    /// 名前を伴う初期化
    public init(name: T) {
        self.name = name
    }
    
}

/// 通知名プロトコル
public protocol LimNotificationNameProtocol {
    
}
