 //
 //  LimNotificationCenter.swift
 //  LimNotificationCenter
 //
 //  Created by Ysk.Manabe on 2017/12/02.
 //  Copyright © 2017年 S4U. All rights reserved.
 //
 
 import Foundation
 
/// 通知センター
 public class LimNotificationCenter<Name: LimNotificationNameProtocol> {
    
    /// 通知センター
    private let notificationCenter = NotificationCenter()
    
    /// オブザーバー管理
    private var observers: [UnsafeRawPointer: [Notification.Name: NSObjectProtocol]] = [:]
    
    public init() {

    }
   
    /// 観測追加
    /// 二重登録不可
    /// - parameter observer: 観測者追加
    /// - parameter queue: キュー nilの場合は通知側のスレッドにて通知が実行される
    /// - parameter notificationHandler: 通知ハンドラー
    public func add<Name>(observer: AnyObject, queue: OperationQueue? = nil, notificationHandler: @escaping (LimNotification<Name>) -> Void) {

        let observerPointer = UnsafeRawPointer(Unmanaged.passUnretained(observer).toOpaque())

        let name = Notification.Name(rawValue: String(describing: Name.self))
        
        // 登録済みの場合は破棄
        if let observerComponent = self.observers[observerPointer] {
            if observerComponent.contains(where: { (k, v) -> Bool in
                return k == name
            }) {
                return
            }
        }
        
        // 通知生成
        let notification: (Notification) -> Void = { (notification: Notification) -> Void in
            let notification = notification.userInfo![name] as! LimNotification<Name>
            notificationHandler(notification)
        }
      
        // 登録
        let token = self.notificationCenter.addObserver(forName: name, object: nil, queue: queue, using: notification)

        // トークンを保存
        switch self.observers.contains(where: { (k, v) -> Bool in
            return k == observerPointer
        }) {
        case true:
            self.observers[observerPointer]![name] = token
        case false:
            self.observers[observerPointer] = [name: token]
        }

    }
    
    /// 通知名を指定した観測削除
    /// - parameter observer: 観測者
    /// - parameter name: 通知名
    public func remove<Name>(observer: AnyObject, name: Name.Type) {
        
        let key = UnsafeRawPointer(Unmanaged.passRetained(observer).toOpaque())
        let name = Notification.Name(rawValue: String(describing: Name.self))
        guard let token = self.observers[key]?[name] else {
            return
        }
        
        // observer登録解除
        self.notificationCenter.removeObserver(token)

        // トークンを破棄
        self.observers[key]?.removeValue(forKey: name)
        if self.observers[key]?.count == 0 {
            self.observers.removeValue(forKey: key)
        }

    }
    
    /// 観測解除
    /// - parameter observer: 観測者
    public func remove(observer: AnyObject) {
        
        let key = Unmanaged.passRetained(observer).toOpaque()
        guard let tokens = self.observers[key] else {
            return
        }
        
        tokens.forEach { (key, value) in
            self.notificationCenter.removeObserver(value)
        }
        
        // observerに紐づいている全てのトークンを破棄
        self.observers.removeValue(forKey: key)
    }
    
    /// 通知投稿
    /// - parameter notificsation: 通知
    public func post<Name>(notification: LimNotification<Name>) {
        let name = Notification.Name(rawValue: String(describing: Name.self))
        self.notificationCenter.post(name: name, object: self, userInfo: [name: notification])
        
    }
    
 }
 
 
